from flask import Flask, request, redirect, url_for, jsonify

app = Flask(__name__)

@app.route('/hello')
def hello_world():
    return 'Hello, welcome to the ESBay User API\n'

@app.route('/bye')
def bye_world():
    return 'bye world\n'

@app.route('/hello/<param1>/<param2>')
def hello_world_name(param1, param2):
    return 'Hello world ' +str(param1) + ' ' + str(param2)

@app.route('/search', methods=['GET'])
def search():
    return request.args.get("name")

@app.route('/<name>')
def hello(name):
    return 'Hello ' + str(name)

@app.route('/')
def redirectTest():
    return redirect(url_for('hello',name='dylan'))

@app.route('/api/add', methods=['GET'])
def add():
 number1 = request.args.get('a', type=int)
 number2 = request.args.get('b', type=int)
 if number1 is None or number2 is None:
    return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
 result = number1 + number2
 return jsonify({'result': result})

@app.route('/api/minus', methods=['GET'])
def minus():
 number1 = request.args.get('a', type=int)
 number2 = request.args.get('b', type=int)
 if number1 is None or number2 is None:
    return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
 result = number1 - number2
 return jsonify({'result': result})

@app.route('/api/multiplication', methods=['GET'])
def multiplication():
 number1 = request.args.get('a', type=int)
 number2 = request.args.get('b', type=int)
 if number1 is None or number2 is None:
    return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
 result = number1 * number2
 return jsonify({'result': result})

@app.route('/api/division', methods=['GET'])
def division():
 number1 = request.args.get('a', type=int)
 number2 = request.args.get('b', type=int)
 if number1 is None or number2 is None:
    return jsonify({'error': 'Invalid input, please provide two numbers'}), 400
 result = number1 / number2
 return jsonify({'result': result})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)