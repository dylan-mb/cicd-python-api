/

/hello

/bye


/hello/<param1>/<param2>
example: /hello/Dylan/Mignot

/search
example: /search?name=Dylan

/<name>
example /Dylan

/api/add
example: /api/add?a=2&b=3

/api/minus
example: /api/minus?a=2&b=4

/api/multiplication
example: /api/multiplication?a=2&b=4

/api/division
example: /api/division?a=4&b=2