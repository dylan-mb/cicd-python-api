import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_hello(client):
    response = client.get("/hello")
    print("msldkjf")
    assert response.status_code == 200
    assert b"Hello, welcome to the ESBay User API\n" in response.data

def test_add(client):
    response = client.get("/api/add?a=1&b=4")
    assert response.status_code == 200
    assert b'"result":5' in response.data

def test_add_invalid_input(client):
    response = client.get("/api/add?a=2&b=g")
    assert response.status_code == 400
    assert b'{"error":"Invalid input, please provide two numbers"}\n' in response.data

def test_minus(client):
    response = client.get("/api/minus?a=1&b=4")
    assert response.status_code == 200
    assert b'"result":-3' in response.data

def test_minus_invalid_input(client):
    response = client.get("/api/minus?a=2&b=")
    assert response.status_code == 400
    assert b'{"error":"Invalid input, please provide two numbers"}\n' in response.data

def test_multiplication(client):
    response = client.get("/api/multiplication?a=2&b=4")
    assert response.status_code == 200
    assert b'"result":8' in response.data

def test_multiplication_invalid_input(client):
    response = client.get("/api/multiplication?a=2&b=")
    assert response.status_code == 400
    assert b'{"error":"Invalid input, please provide two numbers"}\n' in response.data

def test_division(client):
    response = client.get("/api/minus?a=4&b=2")
    assert response.status_code == 200
    assert b'"result":2' in response.data

def test_division_invalid_input(client):
    response = client.get("/api/division?a=2&b=")
    assert response.status_code == 400
    assert b'{"error":"Invalid input, please provide two numbers"}\n' in response.data

def test_division_by_zero(client):
    
    with pytest.raises(ZeroDivisionError):
        response = client.get("/api/division?a=2&b=0")
    # assert response.status_code == 400
    # assert b'{"error":"Invalid input, please provide two numbers"}\n' in response.data
